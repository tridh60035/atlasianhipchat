//
//  BaseObject.m
//  AtlasianHipChat
//
//  Created by TriDH2 on 8/11/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import "BaseObject.h"

@implementation BaseObject

- (instancetype)initWithDict:(NSDictionary *)dictionary
{
    if (self = [super init])
    {
        if (dictionary != nil && [dictionary count] > 0)
        {
            [self setupWithDict:dictionary];
        }
    }
    return self;
}

- (void)setupWithDict:(NSDictionary *)dict
{
    _dataSource = dict;
}

@end
