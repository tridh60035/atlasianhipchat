//
//  CommentObject.m
//  AtlasianHipChat
//
//  Created by MAC on 8/13/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import "CommentObject.h"

@implementation CommentObject

/**
 *  Initialize object
 *
 *  @return An initialized object
 */
- (id)init
{
    self = [super init];
    if (self)
    {
        _shouldShowFullMessage = NO;
        _shouldHideShowMoreButton = NO;
    }
    return self;
}

@end
