//
//  CommentObject.h
//  AtlasianHipChat
//
//  Created by MAC on 8/13/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import "BaseObject.h"

@interface CommentObject : BaseObject

@property (nonatomic) BOOL shouldShowFullMessage;
@property (nonatomic) BOOL shouldHideShowMoreButton;
@property (nonatomic, strong) NSString* message;
@property (nonatomic, strong) NSString* userName;
@property (nonatomic, strong) NSString* timeString;


@end
