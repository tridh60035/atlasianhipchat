//
//  UserObject.h
//  AtlasianHipChat
//
//  Created by TriDH2 on 8/11/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import "BaseObject.h"

@interface UserObject : BaseObject

@property (nonatomic, strong) NSString* userName;

@end
