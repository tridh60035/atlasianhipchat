//
//  SettingObject.m
//  AtlasianHipChat
//
//  Created by MAC on 8/12/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import "SettingObject.h"

@implementation SettingObject

- (id)init
{
    self = [super init];
    if (self)
    {
        _isRememberLogin = [USERDEFAULT boolForKey:kKeySetting_RemeberLogin];
        _userToken = [USERDEFAULT objectForKey:kKeyUserToken];
        _userName = [USERDEFAULT objectForKey:kKeyUserName];
    }
    return self;
}

/**
 *  Override setter method of the variable isRememberLogin sothat each time it change value, it will change the value from the NSUserdefault as well.
 *
 *  @param isRememberLogin New bool value
 */
- (void)setIsRememberLogin:(BOOL)isRememberLogin
{
    _isRememberLogin = isRememberLogin;
    [USERDEFAULT setBool:_isRememberLogin forKey:kKeySetting_RemeberLogin];
    [USERDEFAULT synchronize];
}

/**
 *  Override setter method of the variable userToken sothat each time it change value, it will change the value from the NSUserdefault as well.
 *
 *  @param userToken New String value
 */
- (void)setUserToken:(NSString *)userToken
{
    _userToken = userToken;
    [USERDEFAULT setObject:_userToken forKey:kKeyUserToken];
    [USERDEFAULT synchronize];
}

/**
 *  Override setter method of the variable userName sothat each time it change value, it will change the value from the NSUserdefault as well.
 *
 *  @param userToken New String value
 */
- (void)setUserName:(NSString *)userName
{
    _userName = userName;
    [USERDEFAULT setObject:_userName forKey:kKeyUserName];
    [USERDEFAULT synchronize];
}

@end
