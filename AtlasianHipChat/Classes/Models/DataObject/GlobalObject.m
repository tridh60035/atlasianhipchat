//
//  GlobalObject.m
//  AtlasianHipChat
//
//  Created by MAC on 8/11/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import "GlobalObject.h"

@implementation GlobalObject
@synthesize dataApp, isLogin;


+ (GlobalObject*)shareInstanced
{
    static GlobalObject* sharedObject = nil;
    @synchronized(self)
    {
        if (sharedObject == nil)
        {
            sharedObject = [[self alloc] init];
            sharedObject.setting = [[SettingObject alloc] init];
            sharedObject.dataApp = [[DataApp alloc] init];
        
        }
    }
    

    return sharedObject;
}

- (void)dealloc
{
    dataApp = nil;
}

@end
