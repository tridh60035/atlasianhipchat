//
//  SettingObject.h
//  AtlasianHipChat
//
//  Created by MAC on 8/12/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import "BaseObject.h"

@interface SettingObject : BaseObject

@property (nonatomic) BOOL isRememberLogin;
@property (nonatomic, strong) NSString* userToken;
@property (nonatomic, strong) NSString* userName;

@end
