//
//  DataApp.h
//  AtlasianHipChat
//
//  Created by MAC on 8/11/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserObject.h"

@interface DataApp : NSObject

@property (nonatomic, strong) UserObject* userData;

@end
