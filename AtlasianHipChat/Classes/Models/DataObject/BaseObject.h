//
//  BaseObject.h
//  AtlasianHipChat
//
//  Created by TriDH2 on 8/11/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseObject : NSObject

@property (nonatomic, strong) NSDictionary* dataSource;

/**
 *  Initialize object with the inputted NSDictionary data type
 *
 *  @param dictionary A dictionary containing the keys and values with which to initialize the object
 *
 *  @return An initialized object of this class
 */
- (instancetype)initWithDict:(NSDictionary *)dictionary;

/**
 *  Setup this class with data from inputted NSDictionary data type
 *
 *  @param dict A dictionary containing the keys and values
 */
- (void)setupWithDict:(NSDictionary*)dict;

@end
