//
//  DataApp.m
//  AtlasianHipChat
//
//  Created by MAC on 8/11/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import "DataApp.h"

@implementation DataApp

/**
 *  Initialize object
 *
 *  @return An initialized object
 */
- (id)init
{
    self = [super init];
    if (self)
    {
        _userData = [[UserObject alloc] init];
        if (GLOBALOBJECT.setting.isRememberLogin)
        {
            _userData.userName = GLOBALOBJECT.setting.userName;
        }
    }
    return self;
}

@end
