//
//  GlobalObject.h
//  AtlasianHipChat
//
//  Created by MAC on 8/11/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataApp.h"
#import "SettingObject.h"

@interface GlobalObject : NSObject
{
    
}

@property (nonatomic, strong) DataApp* dataApp;
@property (nonatomic, strong) SettingObject* setting;
@property (nonatomic) BOOL isLogin;

/**
 *  Gets the singleton instance.
 *
 *  @return The singleton object of this class.
 */
+ (GlobalObject*)shareInstanced;

@end
