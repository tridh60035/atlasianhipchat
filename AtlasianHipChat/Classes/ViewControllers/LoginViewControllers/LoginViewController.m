//
//  LoginViewController.m
//  AtlasianHipChat
//
//  Created by TriDH2 on 8/11/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import "LoginViewController.h"
#import "UITextField+Custom.h"

@interface LoginViewController ()
{
    
}
@property (nonatomic, weak) IBOutlet UIScrollView* mainScrollView;
@property (nonatomic, weak) IBOutlet UIView* textFieldView;
@property (nonatomic, weak) IBOutlet UITextField* emailTextField;
@property (nonatomic, weak) IBOutlet UIView* dividerView;
@property (nonatomic, weak) IBOutlet UITextField* passWordTextField;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [Utils setBorderForView:_textFieldView withRadius:2.0f WithColor:[UIColor clearColor] WithShaDow:YES WithCustomShadowColor:[UIColor blackColor]];
    [Utils showAlertWithMessage:[Utils getLocalizedStringWithKey:kKeyLogin_Test_Reminder] AndTitle:@"Warning"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [super registerKeyboardNotification];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [super removeKeyBoardNoptification];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/**
 *  Validate Login Input
 */
- (BOOL)validateLogin
{
    if ([_emailTextField.text isEqualToString:@""])
    {
        [Utils showAlertWithMessage:[Utils getLocalizedStringWithKey:kKeyLogin_Email_Empty] AndTitle:@"Warning"];
        return NO;
    }
    if ([_passWordTextField.text isEqualToString:@""])
    {
        [Utils showAlertWithMessage:[Utils getLocalizedStringWithKey:kKeyLogin_Password_Empty] AndTitle:@"Warning"];
        return NO;
    }
    return YES;
}

#pragma mark - TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    BOOL isValid = [self validateLogin];
    if (isValid)
    {
        DATAAPP.userData.userName = _emailTextField.text;
        GLOBALOBJECT.setting.userName = _emailTextField.text;
        GLOBALOBJECT.setting.userToken = _passWordTextField.text;
        GLOBALOBJECT.setting.isRememberLogin = YES;
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }
    
    return NO;
}

#pragma mark - Keyboard Method

- (void)keyboardWillAppear:(NSNotification*)notification
{
    NSDictionary* info = [notification userInfo];
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    kbRect = [self.view convertRect:kbRect fromView:nil];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbRect.size.height, 0.0);
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
    
    [self.view addGestureRecognizer:self.singleTap];
}

- (void)keyboardWillDisappear:(NSNotification*)notification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
    [self.view removeGestureRecognizer:self.singleTap];
}

#pragma mark - Dealloc

- (void)dealloc
{
    
}

@end
