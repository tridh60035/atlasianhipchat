//
//  MainViewController.m
//  AtlasianHipChat
//
//  Created by MAC on 8/12/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController
static MainViewController* mainVC = nil;


- (void)viewDidLoad {
    [super viewDidLoad];
    mainVC = self;
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    if (GLOBALOBJECT.setting.isRememberLogin)
    {
        [self performSegueWithIdentifier:kSegueMainPushToHomeViewController sender:nil];
        
    }
    else
    {
        //This is a faking behaviour. In real life, we may have some request for loading config, authenticate user or so on in this method.
        [self showLoadingView];
        [self performSelector:@selector(showLoginView) withObject:nil afterDelay:1];
    }
}

/**
 *  This method will present login view
 */
- (void)showLoginView
{
    [self hideLoadingView];
    [self performSegueWithIdentifier:kSegueMainPresentToLoginViewController sender:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+ (MainViewController*)mainViewController
{
    return mainVC;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
