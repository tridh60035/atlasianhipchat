//
//  MainViewController.h
//  AtlasianHipChat
//
//  Created by MAC on 8/12/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import "BaseViewController.h"

@interface MainViewController : BaseViewController

/**
 *  Gets the static instance of this class.
 *
 *  @return The singleton object of this class.
 */
+ (MainViewController*)mainViewController;

@end
