//
//  HomeViewController.h
//  AtlasianHipChat
//
//  Created by MAC on 8/11/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface HomeViewController : BaseViewController<UITextViewDelegate>

@end
