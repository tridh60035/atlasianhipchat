//
//  HomeViewController.m
//  AtlasianHipChat
//
//  Created by MAC on 8/11/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import "HomeViewController.h"
#import "CommentObject.h"
#import "CommentTableViewCell.h"
#import "NSString+HipChatExtension.h"

@interface HomeViewController ()

@property (nonatomic, weak) IBOutlet UIScrollView* mainScrollView;
@property (nonatomic, weak) IBOutlet UITextView* messageTextView;

@property (nonatomic, strong) UITextView* calculateHeightTextView;
@property (nonatomic, weak) IBOutlet UILabel* titleLabel;
@property (nonatomic, weak) IBOutlet UITableView* messageTableView;
@property (nonatomic, weak) IBOutlet UIButton* profileButtonView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint* textViewHeightConstraint;

@property (nonatomic, strong) NSMutableArray* commentArray;
@property (nonatomic, strong) NSOperationQueue* continuousQueue;


@end

@implementation HomeViewController
@synthesize messageTableView, messageTextView, profileButtonView, commentArray;
@synthesize continuousQueue;
#define DEFAULTSYSTEMNAME       @"System Admin"
#define MAXTEXTVIEWHEIGHT       80
#define MINTEXTVIEWHEIGHT       35

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    [self.titleLabel setText:DATAAPP.userData.userName];
    commentArray = [[NSMutableArray alloc] init];
    continuousQueue = [[NSOperationQueue alloc] init];
    continuousQueue.maxConcurrentOperationCount = 1;
    [Utils setBorderForView:self.messageTextView withRadius:2.0f WithColor:[UIColor lightGrayColor] WithShaDow:NO WithCustomShadowColor:nil];
    [messageTableView registerNib:[UINib nibWithNibName:@"CommentTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CommentTableViewCellID"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [super registerKeyboardNotification];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [super removeKeyBoardNoptification];
}

#pragma mark - IBAction and Method

/**
 *  This method will present ProfileViewController
 *
 *  @param sender Receiver of this action
 */
- (IBAction)profileButtonPressed:(id)sender
{
    [self performSegueWithIdentifier:kSegueHomePresentProfile sender:nil];
}

/**
 *  This method will remove all the message
 *
 *  @param sender Receiver of this action.
 */
- (IBAction)clearButtonPressed:(id)sender
{
    [self.commentArray removeAllObjects];
    [self.messageTableView reloadData];
}

/**
 *  This method trigged whenever user press send button. Which will create a message object and an answer object.
 *
 *  @param sender Receiver of this action.
 */
- (IBAction)sendButtonPressed:(id)sender
{
    //if the message textfield is empty. we should not doing anything.
    if ([messageTextView.text isEqualToString:@""])
    {
        return;
    }
    
    NSInvocationOperation *messageOperation = [[NSInvocationOperation alloc] initWithTarget:self
                                                                            selector:@selector(insertMessage)
                                                                              object:nil];
    if (continuousQueue.operations.count > 0)
    {
        [messageOperation addDependency:[self.continuousQueue.operations lastObject]];
    }
    
    [self.continuousQueue addOperation:messageOperation];
    
    
    NSInvocationOperation *answerOperation = [[NSInvocationOperation alloc] initWithTarget:self
                                                                            selector:@selector(insertAnswer)
                                                                              object:nil];
    [answerOperation addDependency:messageOperation];
    [self.continuousQueue addOperation:answerOperation];
}

/**
 *  This method will insert a message in to the message tableview.
 */
- (void)insertMessage
{
    dispatch_async(dispatch_get_main_queue(), ^{
        CommentObject* commentOBJ = [self createAMessageObjectIsAnswer:NO];
        [commentArray addObject:commentOBJ];
        [messageTableView beginUpdates];
        [messageTableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:(commentArray.count - 1) inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        [messageTableView endUpdates];
        CommentTableViewCell* cell = (CommentTableViewCell*)[messageTableView dequeueReusableCellWithIdentifier:@"CommentTableViewCellID"];
        float heightCell = [cell calculateSizeWithData:commentOBJ];
        if (messageTableView.contentSize.height + heightCell > messageTableView.frame.size.height - _mainScrollView.contentOffset.y)
        {
            CGPoint offset = CGPointMake(0, messageTableView.contentSize.height + heightCell - messageTableView.frame.size.height);
            [messageTableView setContentOffset:offset animated:YES];
        }
    });
}

/**
 *  This method will insert answer for the previous message into the message table view.
 */
- (void)insertAnswer
{
    dispatch_async(dispatch_get_main_queue(), ^{
        CommentObject* answertMessage = [self createAMessageObjectIsAnswer:YES];
        messageTextView.text = @"";
        [commentArray addObject:answertMessage];
        self.textViewHeightConstraint.constant = MINTEXTVIEWHEIGHT;
        [self.mainScrollView layoutIfNeeded];
        [messageTableView beginUpdates];
        [messageTableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:(commentArray.count - 1) inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        [messageTableView endUpdates];
        CommentTableViewCell* cell = (CommentTableViewCell*)[messageTableView dequeueReusableCellWithIdentifier:@"CommentTableViewCellID"];
        
        float heightCell = [cell calculateSizeWithData:answertMessage];
        if (messageTableView.contentSize.height + heightCell > messageTableView.frame.size.height - _mainScrollView.contentOffset.y)
        {
            CGPoint offset = CGPointMake(0, messageTableView.contentSize.height + heightCell - messageTableView.frame.size.height);
            [messageTableView setContentOffset:offset animated:YES];
        }
    });
}

/**
 *  Create and returm a messageObject
 *
 *  @param isAnswer Bool Value indicate if this messageObject is come from System or not.
 *
 *  @return The return message object.
 */
- (CommentObject*)createAMessageObjectIsAnswer:(BOOL)isAnswer
{
    CommentObject* messageOBJ = [[CommentObject alloc] init];
    if (isAnswer)
    {
        messageOBJ.message = [messageTextView.text jsonStringFromChatString];
        messageOBJ.userName = DEFAULTSYSTEMNAME;
    }
    else
    {
        messageOBJ.message = messageTextView.text;
        messageOBJ.userName = DATAAPP.userData.userName;
    }
    
    messageOBJ.timeString = [Utils getCurrentTimeString];
    return messageOBJ;
}

#pragma mark - TextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
    NSString* resultString = [textView.text stringByReplacingCharactersInRange:range withString:text];
 
    if (!self.calculateHeightTextView)
    {
        self.calculateHeightTextView = [[UITextView alloc] initWithFrame:self.messageTextView.frame];
        self.calculateHeightTextView.font = self.messageTextView.font;
    }
    self.calculateHeightTextView.text = resultString;
    CGSize expectedSize = [self.calculateHeightTextView sizeThatFits:CGSizeMake(self.messageTextView.frame.size.width, FLT_MAX)];

    if (expectedSize.height > MINTEXTVIEWHEIGHT)
    {
        if(expectedSize.height < MAXTEXTVIEWHEIGHT)
        {
            self.textViewHeightConstraint.constant = expectedSize.height;
        }
        else
        {
            self.textViewHeightConstraint.constant = MAXTEXTVIEWHEIGHT;
        }
    }
    else
    {
        self.textViewHeightConstraint.constant = MINTEXTVIEWHEIGHT;
    }
    [self.mainScrollView layoutIfNeeded];
    return YES;
}

#pragma mark - Keyboard Method

- (void)keyboardWillAppear:(NSNotification*)notification
{
    NSDictionary* info = [notification userInfo];
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    kbRect = [self.view convertRect:kbRect fromView:nil];
    
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbRect.size.height;
    [self.view setFrame:aRect];
    [self.view addGestureRecognizer:self.singleTap];
}

- (void)keyboardWillDisappear:(NSNotification*)notification
{
    CGRect aRect = self.view.frame;
    aRect.size.height = screenDeviceRect.size.height;
    [self.view setFrame:aRect];
    [self.view removeGestureRecognizer:self.singleTap];
}

#pragma mark - TableView DataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommentTableViewCell* cell = (CommentTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"CommentTableViewCellID"];
    CommentObject* curCommentOBJ = commentArray[indexPath.row];
    [cell updateDataWith:curCommentOBJ];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.showMoreButton.tag = [indexPath row];
    [cell.showMoreButton addTarget:self action:@selector(showMoreButtonPress:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return commentArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommentObject* curCommentOBJ = commentArray[indexPath.row];
    CommentTableViewCell* cell = (CommentTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"CommentTableViewCellID"];
    return  [cell calculateSizeWithData:curCommentOBJ];
}

/**
 *  This method will update if the message will show all of it content or hide part of it.
 *
 *  @param sender Object that take the action.
 */
- (void)showMoreButtonPress:(id)sender
{
    UIButton* curButton = (UIButton*)sender;
    CommentObject* curCommentOBJ = commentArray[curButton.tag];
    curCommentOBJ.shouldShowFullMessage = !curCommentOBJ.shouldShowFullMessage;
    [messageTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:curButton.tag inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
