//
//  BaseViewController.m
//  AtlasianHipChat
//
//  Created by TriDH2 on 8/11/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shouldDissmissKeyBoard)];
    [_singleTap setNumberOfTapsRequired:1];
    
    _loadingIndicatorView = [[UIActivityIndicatorView alloc] initWithFrame:screenDeviceRect];
    _loadingIndicatorView.hidesWhenStopped = YES;
    [self.view addSubview:_loadingIndicatorView];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Loading Indicator Method

- (void)showLoadingView
{
    [self.view bringSubviewToFront:_loadingIndicatorView];
    [_loadingIndicatorView startAnimating];
}

- (void)hideLoadingView
{
    [_loadingIndicatorView stopAnimating];
}

#pragma mark - Register KeyBoard Method Helper

- (void)registerKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillAppear:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillDisappear:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)removeKeyBoardNoptification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillAppear:(NSNotification*)notification
{
    
}

- (void)keyboardWillDisappear:(NSNotification*)notification
{

}

/**
 *  Dissmiss the keyboard.
 */
- (void)shouldDissmissKeyBoard
{
    [self.view endEditing:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
