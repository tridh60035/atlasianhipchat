//
//  BaseViewController.h
//  AtlasianHipChat
//
//  Created by TriDH2 on 8/11/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

@property (nonatomic, strong) UIActivityIndicatorView* loadingIndicatorView;
@property (nonatomic, strong) UITapGestureRecognizer *singleTap;

/**
 *  Show loading indicator
 */
- (void)showLoadingView;

/**
 *  Hide Loading Indicator
 */
- (void)hideLoadingView;

/**
 *  This method will register Keyboard show and hide notification.
 */
- (void)registerKeyboardNotification;

/**
 *  this method will remove the keyboard show and hide notification.
 */
- (void)removeKeyBoardNoptification;

/**
 *  This method will take the action whenever this class receive keyboardWillShow notification
 *
 *  @param notification the received Keyboard show notification
 */
- (void)keyboardWillAppear:(NSNotification*)notification;

/**
 *  This method will take the action whenever this class receive keyboardWillHide notification
 *
 *  @param notification the received Keyboard Hide notification
 */
- (void)keyboardWillDisappear:(NSNotification*)notification;

@end
