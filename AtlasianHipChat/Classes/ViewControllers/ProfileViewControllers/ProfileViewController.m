//
//  ProfileViewController.m
//  AtlasianHipChat
//
//  Created by MAC on 8/15/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import "ProfileViewController.h"
#import "HomeViewController.h"

@interface ProfileViewController ()

@property (nonatomic, weak) IBOutlet UIImageView* profileImageView;
@property (nonatomic, weak) IBOutlet UITextField* userNameTextField;
@property (nonatomic, weak) IBOutlet UITextField* userPasswordTextField;
@property (nonatomic, weak) IBOutlet UIScrollView* mainScrollView;
@property (nonatomic, weak) IBOutlet UIButton* logoutButton;
@property (nonatomic, weak) IBOutlet UIView* userNameView;
@property (nonatomic, weak) IBOutlet UIView* userPassView;

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [Utils setBorderForView:self.userNameView withRadius:4.0f WithColor:[UIColor blackColor] WithShaDow:YES WithCustomShadowColor:[UIColor blackColor]];
    [Utils setBorderForView:self.userPassView withRadius:4.0f WithColor:[UIColor blackColor] WithShaDow:YES WithCustomShadowColor:[UIColor blackColor]];
    [Utils setBorderForView:self.logoutButton withRadius:4.0f WithColor:self.logoutButton.backgroundColor WithShaDow:YES WithCustomShadowColor:self.logoutButton.backgroundColor];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.userNameTextField.text = DATAAPP.userData.userName;
    self.userPasswordTextField.text = GLOBALOBJECT.setting.userToken;
    [super registerKeyboardNotification];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [super removeKeyBoardNoptification];
}

#pragma mark - IBACtion

/**
 *  Handle Lgout action
 *
 *  @param sender Receiver of this action.
 */
- (IBAction)logoutButtonPressed:(id)sender
{
    GLOBALOBJECT.setting.isRememberLogin = NO;
    GLOBALOBJECT.setting.userName = @"";
    GLOBALOBJECT.setting.userToken = @"";
    [self dismissViewControllerAnimated:YES completion:^{
        [[MainViewController mainViewController].navigationController popViewControllerAnimated:YES];
    }];
}

/**
 *  Dissmiss ProfileViewController
 *
 *  @param sender Receiver of this action
 */
- (IBAction)backButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Keyboard Method

- (void)keyboardWillAppear:(NSNotification*)notification
{
    NSDictionary* info = [notification userInfo];
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    kbRect = [self.view convertRect:kbRect fromView:nil];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbRect.size.height, 0.0);
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
    
    [self.view addGestureRecognizer:self.singleTap];
}

- (void)keyboardWillDisappear:(NSNotification*)notification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _mainScrollView.contentInset = contentInsets;
    _mainScrollView.scrollIndicatorInsets = contentInsets;
    [self.view removeGestureRecognizer:self.singleTap];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
