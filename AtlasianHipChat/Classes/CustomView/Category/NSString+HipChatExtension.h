//
//
//  NSString+HipChatExtension.h
//  AtlasianHipChat
//
//  Created by TriDH2 on 8/11/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (HipChatExtension)
/**
 *  Analyze and convert chat message string into JSON format string
 *
 *  @return json format string
 */
- (NSString *)jsonStringFromChatString;
@end
