//
//
//  NSString+HipChatExtension.m
//  AtlasianHipChat
//
//  Created by TriDH2 on 8/11/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import "NSString+HipChatExtension.h"
#import "MessageDetectedText.h"
#import "MessageDetectedObject.h"

#define kRegex_Non_Word @"\z[^a-zA-Z0-9-]+"
#define kRegex_Emoticon @"[^a-zA-Z0-9-]+"

@implementation NSString (HipChatExtension)

/**
 *  Analyze and convert chat message string into JSON format string
 *
 *  @return json format string
 */
- (NSString *)jsonStringFromChatString;
{
    // Validate input
    if ( ! ([self length] > 0))
    {
        return nil;
    }
    
    // Analyze Mention data
    NSDictionary * mentionsInfo     = [self analyzeMentionsData];

    // Analyze Emoticons data
    NSDictionary * emoticonsInfo    = [self analyzeEmoticonsData];

    // Analyze Links data
    NSDictionary * linksInfo        = [self analyzeLinksData];
    
    // Combine all data into 1 result dictionary
    NSMutableDictionary * resultDict = [NSMutableDictionary dictionary];
    [resultDict addEntriesFromDictionary:mentionsInfo];
    [resultDict addEntriesFromDictionary:emoticonsInfo];
    [resultDict addEntriesFromDictionary:linksInfo];
    
    // Convert result dict into JSON string
    if ([NSJSONSerialization isValidJSONObject:resultDict])
    {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:resultDict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        
        if (! jsonData)
        {
            NSLog(@"Convert into JSON string failed with error: %@", error.localizedDescription);
            return nil;
        }
        else
        {
            NSString * jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
            return jsonStr;
        }
    }
    return nil;
}

/**
 *  Analyze and convert chat mention symbol string into NSDictionary
 *
 *  @return NSDictionary object that contains all the mention symbol
 */
- (NSDictionary *)analyzeMentionsData
{
    // Validate input
    if ( ! ([self length] > 0))
    {
        return nil;
    }
    
    NSArray * entities = [MessageDetectedText mentionedNamesInText:self];
    
    NSMutableArray * mentionResults = [NSMutableArray array];
    for (MessageDetectedObject * entity in entities)
    {
        NSString * mentionGuy = [self substringWithRange:NSMakeRange(entity.messageRange.location + 1, entity.messageRange.length -1)];
        
        
        if (mentionGuy != nil)
        {
            [mentionResults addObject:mentionGuy];
        }
    }
    
    if ([mentionResults count] > 0)
    {
        return @{@"mentions":mentionResults};
    }
    
    return nil;
}

/**
 *  Analyze and convert Emotions icon symbol into NSDictionary
 *
 *  @return NSDictionary object that contains all the Emotions icon symbol
 */
- (NSDictionary *)analyzeEmoticonsData
{
    // Validate input
    if ( ! ([self length] > 0))
    {
        return nil;
    }
    
    NSArray * entities = [MessageDetectedText extractEmoticonsInText:self];
    
    NSMutableArray * emoticonResults = [NSMutableArray array];
    for (MessageDetectedObject * entity in entities)
    {
        NSString * emo = [self substringWithRange:entity.messageRange];
        emo = [emo stringByReplacingOccurrencesOfString:@"(" withString:@""];
        emo = [emo stringByReplacingOccurrencesOfString:@")" withString:@""];
        if (emo != nil)
        {
            [emoticonResults addObject:emo];
        }
    }
    
    if ([emoticonResults count] > 0)
    {
        return @{@"emoticons":emoticonResults};
    }
    
    return nil;
}

/**
 *  Analyze and convert links symbol and extract its title into NSDictionary
 *
 *  @return NSDictionary object that contains all the links symbol and its title
 */
- (NSDictionary *)analyzeLinksData
{
    // Validate input
    if ( ! ([self length] > 0))
    {
        return nil;
    }

    NSArray * entities = [MessageDetectedText extractURLsInText:self];
    
    NSMutableArray * resultArr = [NSMutableArray array];
    for (MessageDetectedObject * entity in entities)
    {
        NSString * urlStr = [self substringWithRange:entity.messageRange];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url];
        NSURLResponse * response;
        NSError * error;
        NSData * resultData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSString * htmlStr = [[NSString alloc] initWithData:resultData encoding:NSUTF8StringEncoding];
        if ([htmlStr length] > 0)
        {
            NSRange titleStartTagRange  = [htmlStr rangeOfString:@"<title>"];
            NSRange titleEndTagRange    = [htmlStr rangeOfString:@"</title>"];
            NSString * title = [htmlStr substringWithRange:NSMakeRange((titleStartTagRange.location + titleStartTagRange.length),titleEndTagRange.location - (titleStartTagRange.location + titleStartTagRange.length))];
            if ([title length] > 0)
            {
                NSDictionary * linkInfo = @{@"url":urlStr,@"title":title};
                [resultArr addObject:linkInfo];
            }
        }
    }
    
    if ([resultArr count] > 0)
    {
        return @{@"links":resultArr};
    }
    return nil;
}




@end
