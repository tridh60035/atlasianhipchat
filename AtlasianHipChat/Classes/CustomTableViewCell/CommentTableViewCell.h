//
//  CommentTableViewCell.h
//  AtlasianHipChat
//
//  Created by MAC on 8/13/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface CommentTableViewCell : BaseTableViewCell

@property (nonatomic, weak) IBOutlet UILabel* userNameLabel;
@property (nonatomic, weak) IBOutlet UILabel* timeLabel;
@property (nonatomic, weak) IBOutlet UILabel* messageLabel;
@property (nonatomic, weak) IBOutlet UIButton* showMoreButton;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint* showMoreHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint* messageBottomConstraint;

@end
