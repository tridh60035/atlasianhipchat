//
//  BaseTableViewCell.h
//  AtlasianHipChat
//
//  Created by MAC on 8/13/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableViewCell : UITableViewCell

/**
 *  Update Cell with the inputted Data
 *
 *  @param data Inputted Data
 */
- (void)updateDataWith:(id)data;

/**
 *  Calculate cell's height with the inputted Data.
 *
 *  @param data Inputted Data
 *
 *  @return The Cell's Heoght
 */
- (CGFloat)calculateSizeWithData:(id)data;

/**
 *  Clear all the data
 */
- (void)clearData;

@end
