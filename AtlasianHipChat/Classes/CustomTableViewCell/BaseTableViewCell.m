//
//  BaseTableViewCell.m
//  AtlasianHipChat
//
//  Created by MAC on 8/13/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import "BaseTableViewCell.h"

@implementation BaseTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateDataWith:(id)data
{
    
}

- (CGFloat)calculateSizeWithData:(id)data
{
    return 0;
}

- (void)clearData
{
    
}

@end
