//
//  CommentTableViewCell.m
//  AtlasianHipChat
//
//  Created by MAC on 8/13/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import "CommentTableViewCell.h"
#import "CommentObject.h"

@implementation CommentTableViewCell
@synthesize userNameLabel, timeLabel, showMoreButton, messageLabel;
@synthesize showMoreHeightConstraint, messageBottomConstraint;
#define ExpectedHeight  200
#define BufferHeight    50
#define ButtonShowMoreDefaultHeight     30
#define messageBottomExpectedMargin     20

/**
 *  Update Cell with the inputted Data
 *
 *  @param data Inputted Data
 */
- (void)updateDataWith:(id)data
{
    CommentObject* curComment = (CommentObject*)data;
    [self clearData];
    [self calculateSizeWithData:data];
    if (curComment.shouldShowFullMessage)
    {
        if (curComment.shouldHideShowMoreButton)
        {
            //This case show full message without show less button
            showMoreHeightConstraint.constant = 0;
            [showMoreButton setTitle:@"" forState:UIControlStateNormal];
            messageBottomConstraint.constant = messageBottomExpectedMargin;
        }
        else
        {
            //This case show full message with show less button
            showMoreHeightConstraint.constant = ButtonShowMoreDefaultHeight;
            messageBottomConstraint.constant = 0;
            [showMoreButton setTitle:@"Show Less" forState:UIControlStateNormal];
        }
        
    }
    else
    {
        //Only 1 case in here: text too long
        showMoreHeightConstraint.constant = ButtonShowMoreDefaultHeight;
        messageBottomConstraint.constant = 0;
        [showMoreButton setTitle:@"Show More" forState:UIControlStateNormal];
    }
}

/**
 *  Calculate cell's height with the inputted Data.
 *
 *  @param data Inputted Data
 *
 *  @return The Cell's Heoght
 */
- (CGFloat)calculateSizeWithData:(id)data
{
    CommentObject* curComment = (CommentObject*)data;
    userNameLabel.text = curComment.userName;
    timeLabel.text = curComment.timeString;
    messageLabel.text = curComment.message;
    float actualHeight = 0;
    float userNameTopMargin = 15;
    float messageTopMarginWithUserName = 10;
    float messageMarginLeft = 20;
    float messageMarginRight = 20;
    float messageWidth = ApplicationRect.size.width - messageMarginLeft - messageMarginRight;
    CGSize userNameSize = [self.timeLabel sizeThatFits:CGSizeMake(100, FLT_MAX)];
    CGSize expectedSize = [self.messageLabel sizeThatFits:CGSizeMake(messageWidth, FLT_MAX)];
    
    actualHeight = (userNameTopMargin + messageTopMarginWithUserName + expectedSize.height + userNameSize.height);
    
    if (actualHeight > (ExpectedHeight + BufferHeight))
    {
        if (curComment.shouldShowFullMessage)
        {
            if (curComment.shouldHideShowMoreButton)
            {
                //This case show full message without show less button
                actualHeight += messageBottomExpectedMargin;
                return actualHeight;
            }
            else
            {
                actualHeight += ButtonShowMoreDefaultHeight;
                return actualHeight;
            }
        }
        else
        {
            return ExpectedHeight;
        }
    }
    else
    {
        curComment.shouldShowFullMessage = YES;
        curComment.shouldHideShowMoreButton = YES;
        actualHeight += messageBottomExpectedMargin;
        return actualHeight ;
    } 
    return actualHeight;
}

/**
 *  Clear all the data
 */
- (void)clearData
{
    userNameLabel.text = @"";
    timeLabel.text = @"";
    messageLabel.text = @"";

}

@end
