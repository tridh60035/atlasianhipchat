//
//  MessageDetectedText.h
//  AtlasianHipChat
//
//  Created by MAC on 8/15/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import "BaseObject.h"
#import "MessageDetectedObject.h"

typedef NS_ENUM(NSUInteger, MessageTextRegularExpressionType) {
    MessageTextRegularExpressionTypeMention,
    MessageTextRegularExpressionTypeEmoticons,
    MessageTextRegularExpressionTypeEnd
};


@interface MessageDetectedText : BaseObject

/**
 *  This method will identify the mention name and it's range from the inputted text.
 *
 *  @param text inputted text
 *
 *  @return An array of MessageDetectedObject that contain the range of the mention names.
 */
+ (NSArray *)mentionedNamesInText:(NSString *)text;

/**
 *  This method will identify the mention name and it's range from the inputted text.
 *
 *  @param text inputted text
 *
 *  @return An array of MessageDetectedObject that contain the range of the mention names.
 */
+ (NSArray *)extractMentionFromText:(NSString *)text;

/**
 *  This method will identify the emoticons and it's range from the inputted text.
 *
 *  @param text inputted text
 *
 *  @return An array of MessageDetectedObject that contain the range of the emoticons.
 */
+ (NSArray *)extractEmoticonsInText:(NSString *)text;

/**
 *  This method will identify all the link and it's range from the inputted text.
 *
 *  @param text inputted text
 *
 *  @return An array of MessageDetectedObject that contain the range of the links.
 */
+ (NSArray *)extractURLsInText:(NSString *)text;

@end
