//
//  MessageDetectedObject.h
//  AtlasianHipChat
//
//  Created by MAC on 8/15/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import "BaseObject.h"

typedef NS_ENUM(NSUInteger, MessageDetectedObjectType) {
    MessageObjectTypeMention,
    MessageObjectTypeEmoticons,
    MessageObjectTypeLinks,
    MessageObjectTypeList
};

@interface MessageDetectedObject : BaseObject

@property (nonatomic) MessageDetectedObjectType messageType;
@property (nonatomic) NSRange messageRange;


/**
 *  This method return a MessageDetectedObject with the selected Type
 *
 *  @param type  The inputted Type
 *  @param range The range of the detected String
 *
 *  @return An instance type of this object that have the inputted type and range.
 */
+ (instancetype)messageObjectWithType:(MessageDetectedObjectType)type range:(NSRange)range;

@end
