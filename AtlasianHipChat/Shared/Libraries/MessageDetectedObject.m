//
//  MessageDetectedObject.m
//  AtlasianHipChat
//
//  Created by MAC on 8/15/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import "MessageDetectedObject.h"

@implementation MessageDetectedObject
@synthesize messageRange, messageType;

/**
 *  Initialize object with the selected type and Range
 *
 *  @param type  The inputted Type
 *  @param range The range of the detected String
 *
 *  @return <#return value description#>
 */
- (instancetype)initWithType:(MessageDetectedObjectType)type range:(NSRange)range
{
    self = [super init];
    if (self) {
        self.messageType = type;
        self.messageRange = range;
    }
    return self;
}

+ (instancetype)messageObjectWithType:(MessageDetectedObjectType)type range:(NSRange)range
{
    MessageDetectedObject *entity = [[self alloc] initWithType:type range:range];
    return entity;
}

@end
