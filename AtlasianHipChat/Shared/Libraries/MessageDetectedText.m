//
//  MessageDetectedText.m
//  AtlasianHipChat
//
//  Created by MAC on 8/15/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import "MessageDetectedText.h"

// No longer than 15 Ascii characters, contained in parenthesis
#define HD_Emoticons_Character @"\\([a-zA-Z0-9]{0,15}\\)"


#define HD_EndCharacterRegex \
@"\\u00C0-\\u00D6\\u00D8-\\u00F6\\u00F8-\\u00FF\\u0100-\\u024F\\u0253-\\u0254\\u0256-\\u0257\\u0259\\u025b\\u0263\\u0268\\u026F\\u0272\\u0289\\u02BB\\u1E00-\\u1EFF"

#define HD_ValidMentionRegex \
@"((?:[^a-zA-Z0-9_!#$%&*@＠]|^|RT:?))" \
@"([@＠])" \
@"([a-zA-Z0-9_]{1,20})" \
@"(/[a-zA-Z][a-zA-Z0-9_\\-]{0,24})?"

#define HD_EndMentionRegex              @"\\A(?:[@＠]|[" HD_EndCharacterRegex @"]|://)"

@implementation MessageDetectedText

+ (NSArray *)mentionedNamesInText:(NSString *)text
{
    if (!text.length) {
        return [NSArray array];
    }
    
    NSArray *mentionsOrLists = [self extractMentionFromText:text];
    NSMutableArray *results = [NSMutableArray array];
    
    for (MessageDetectedObject *object in mentionsOrLists)
    {
        if (object.messageType == MessageObjectTypeMention)
        {
            [results addObject:object];
        }
    }
    
    return results;
}

+ (NSArray *)extractMentionFromText:(NSString *)text
{
    if (!text.length) {
        return [NSArray array];
    }
    
    NSMutableArray *results = [NSMutableArray array];
    NSUInteger len = text.length;
    NSUInteger position = 0;
    
    while (1) {
        NSTextCheckingResult *matchResult = [[self validMentionOrListRegexp] firstMatchInString:text options:NSMatchingWithoutAnchoringBounds range:NSMakeRange(position, len - position)];
        if (!matchResult || matchResult.numberOfRanges < 5) {
            break;
        }
        
        NSRange allRange = matchResult.range;
        NSUInteger end = NSMaxRange(allRange);
        
        NSRange endMentionRange = [[self endMentionRegexp] rangeOfFirstMatchInString:text options:0 range:NSMakeRange(end, len - end)];
        if (endMentionRange.location == NSNotFound) {
            NSRange atSignRange = [matchResult rangeAtIndex:2];
            NSRange screenNameRange = [matchResult rangeAtIndex:3];
            NSRange listNameRange = [matchResult rangeAtIndex:4];
            
            if (listNameRange.location == NSNotFound) {
                MessageDetectedObject *entity = [MessageDetectedObject messageObjectWithType:MessageObjectTypeMention range:NSMakeRange(atSignRange.location, NSMaxRange(screenNameRange) - atSignRange.location)];
                [results addObject:entity];
            } else {
                MessageDetectedObject *entity = [MessageDetectedObject messageObjectWithType:MessageObjectTypeList range:NSMakeRange(atSignRange.location, NSMaxRange(listNameRange) - atSignRange.location)];
                [results addObject:entity];
            }
        } else {
            // Avoid matching the second username in @username@username
            end++;
        }
        
        position = end;
    }
    
    return results;
}

+ (NSArray *)extractEmoticonsInText:(NSString *)text
{
    if (!text.length) {
        return [NSArray array];
    }
    
    NSMutableArray *results = [NSMutableArray array];
    NSUInteger len = text.length;
    NSUInteger position = 0;
    
    while (1) {
        NSTextCheckingResult *matchResult = [[self validEmoticonsRegex] firstMatchInString:text options:NSMatchingWithoutAnchoringBounds range:NSMakeRange(position, len - position)];
        
        if (!matchResult)
        {
            break;
        }
        MessageDetectedObject *entity = [MessageDetectedObject messageObjectWithType:MessageObjectTypeEmoticons range:matchResult.range];
        [results addObject:entity];
        position = NSMaxRange(matchResult.range);
    }
    
    return results;
}

+ (NSArray *)extractURLsInText:(NSString *)text
{
    if (!text.length) {
        return [NSArray array];
    }
    NSDataDetector *detect = [[NSDataDetector alloc] initWithTypes:NSTextCheckingTypeLink error:nil];
    NSArray *matches = [detect matchesInString:text options:0 range:NSMakeRange(0, [text length])];
    NSMutableArray* messageObjectArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < matches.count; i++)
    {
        NSTextCheckingResult* curResult = matches[i];
        MessageDetectedObject* messageObject = [MessageDetectedObject messageObjectWithType:MessageObjectTypeLinks range:curResult.range];
        [messageObjectArray addObject:messageObject];
    }
    return messageObjectArray;
}

#pragma mark - Regular Expresion method

+ (NSRegularExpression*)validMentionOrListRegexp
{
    static NSRegularExpression *regexp;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        regexp = [[NSRegularExpression alloc] initWithPattern:HD_ValidMentionRegex options:NSRegularExpressionCaseInsensitive error:NULL];
    });
    return regexp;
}

+ (NSRegularExpression*)endMentionRegexp
{
    static NSRegularExpression *regexp;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        regexp = [[NSRegularExpression alloc] initWithPattern:HD_EndMentionRegex options:NSRegularExpressionCaseInsensitive error:NULL];
    });
    return regexp;
}

+ (NSRegularExpression *)validEmoticonsRegex
{
    static NSRegularExpression *regexp;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        regexp = [[NSRegularExpression alloc] initWithPattern:HD_Emoticons_Character options:NSRegularExpressionCaseInsensitive error:NULL];
    });
    return regexp;
}

@end
