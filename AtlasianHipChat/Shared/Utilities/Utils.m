//
//  Utils.m
//  AtlasianHipChat
//
//  Created by TriDH2 on 8/11/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+ (void)setBorderForView:(UIView*)inputtedView withRadius:(float)radius WithColor:(UIColor*)color WithShaDow:(BOOL)shouldShadow WithCustomShadowColor:(UIColor*)shadowColor
{
    [inputtedView.layer setCornerRadius:radius];
    
    // border
    
    UIColor* selectedBorderColor = color;
    if (!selectedBorderColor)
    {
        selectedBorderColor = [UIColor clearColor];
    }
    
    [inputtedView.layer setBorderColor:color.CGColor];
    [inputtedView.layer setBorderWidth:1.f];
    
    // drop shadow
    if (shouldShadow)
    {
        UIColor* selectedShadowColor = shadowColor;
        if(selectedShadowColor)
        {
            selectedShadowColor = [UIColor blackColor];
        }
        [inputtedView.layer setShadowColor:selectedShadowColor.CGColor];
        [inputtedView.layer setShadowOpacity:0.5];
        [inputtedView.layer setShadowRadius:1.0];
        [inputtedView.layer setShadowOffset:CGSizeMake(0.5, 0.5)];
    }
}

+ (void)showAlertWithMessage:(NSString*)message AndTitle:(NSString*)title
{
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
}

+ (NSString*)getLocalizedStringWithKey:(NSString*)key
{
    NSString* returnKey = NSLocalizedStringFromTable(key, @"LocalizedString", nil);
    return returnKey;
}

+ (NSString*)getCurrentTimeString
{
    NSDate* thisDate = [NSDate date];
    NSDateFormatter* shortTimeFormatter = [[NSDateFormatter alloc] init];
    shortTimeFormatter.AMSymbol = @"AM";
    shortTimeFormatter.PMSymbol = @"PM";
    shortTimeFormatter.dateFormat = @"h':'mm a";
    NSString* dateString = [shortTimeFormatter stringFromDate:thisDate];
    return dateString;
}

@end
