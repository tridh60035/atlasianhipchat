//
//  Utils.h
//  AtlasianHipChat
//
//  Created by TriDH2 on 8/11/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

/**
 *  This method set border and shadow for the inputted view.
 *
 *  @param inputtedView The inputted view
 *  @param radius       Radius of the border
 *  @param color        Color of the border
 *  @param shouldShadow This attribute indicate that we should set shadow for the border or not.
 *  @param shadowColor  The shadow color for the border view.
 */
+ (void)setBorderForView:(UIView*)inputtedView withRadius:(float)radius WithColor:(UIColor*)color WithShaDow:(BOOL)shouldShadow WithCustomShadowColor:(UIColor*)shadowColor;

/**
 *  Show alert with inputted message and tile
 *
 *  @param message inputted message
 *  @param title   inputted title
 */
+ (void)showAlertWithMessage:(NSString*)message AndTitle:(NSString*)title;

/**
 *  Get Localized string with inputted key
 *
 *  @param key inputted key
 */
+ (NSString*)getLocalizedStringWithKey:(NSString*)key;

/**
 *  Get Current TimeString
 *
 *  @return Current time string.
 */
+ (NSString*)getCurrentTimeString;

@end
