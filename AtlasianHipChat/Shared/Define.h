//
//  Define.h
//  AtlasianHipChat
//
//  Created by MAC on 8/11/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#ifndef AtlasianHipChat_Define_h
#define AtlasianHipChat_Define_h


#endif

//Login Key
#define kKeyLogin_Email_Empty                       @"login_email_empty"
#define kKeyLogin_Password_Empty                    @"login_password_empty"
#define kKeyLogin_Test_Reminder                     @"login_test_reminder"

//Setting Key - NSUserDefault Key
#define kKeySetting_RemeberLogin                    @"isRememberLogin"
#define kKeyUserName                                @"userName"
#define kKeyUserToken                               @"userToken"

//Segue
#define kSegueMainPresentToLoginViewController          @"SegueMainPushToLoginViewController"
#define kSegueMainPushToHomeViewController              @"SegueMainPushToHomeViewController"

//Home Segue
#define kSegueHomePresentProfile                        @"SegueHomePresentProfile"

