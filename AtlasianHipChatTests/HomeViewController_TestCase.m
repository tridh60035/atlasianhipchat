//
//  HomeViewController_TestCase.m
//  AtlasianHipChat
//
//  Created by MAC on 8/13/15.
//  Copyright (c) 2015 Atlasian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "HomeViewController.h"
#import "CommentObject.h"

@interface HomeViewController_TestCase : XCTestCase


@property (nonatomic) HomeViewController* homeVCTestCase;

@end

@interface HomeViewController (Test)

@property (nonatomic, weak) IBOutlet UIScrollView* mainScrollView;
@property (nonatomic, weak) IBOutlet UITextField* messageTextView;
@property (nonatomic, weak) IBOutlet UITableView* messageTableView;
@property (nonatomic, weak) IBOutlet UIButton* profileButtonView;
@property (nonatomic, strong) NSMutableArray* commentArray;
@property (nonatomic, strong) NSOperationQueue* continuousQueue;

- (void)insertAnswer;
- (void)insertMessage;
- (IBAction)sendButtonPressed:(id)sender;
- (CommentObject*)createAMessageObjectIsAnswer:(BOOL)isAnswer;

@end

@implementation HomeViewController_TestCase

- (void)setUp {
    [super setUp];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.homeVCTestCase = [storyboard instantiateViewControllerWithIdentifier:@"HomeViewControllerID"];
    [self.homeVCTestCase loadView];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testPerformanceInsertAnswer
{
    self.homeVCTestCase.messageTextView.text = @"@chris you around?";
    [self measureBlock:^{
        [self.homeVCTestCase sendButtonPressed:nil];
    }];
}

#pragma mark - Mention Test Case

/**
 *  This Test Case is for Mention
 
    Inputted:@chris you around
    Expected Result:    
                    {
                        "mentions" : [
                        "chris"
                        ]
                    }
 
 */
- (void)testMentionChatMessageDefaultCase
{
    self.homeVCTestCase.messageTextView.text = @"@chris you around?";
    CommentObject* curCommentObject = [self.homeVCTestCase createAMessageObjectIsAnswer:YES];

    NSDictionary* resultDict = [NSDictionary dictionaryWithObjectsAndKeys:@[@"chris"], @"mentions", nil];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:resultDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString * expectedString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding
                                 ];
    expectedString = [expectedString stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
    XCTAssertEqualObjects(expectedString, curCommentObject.message, @"Data Match!!!");
}

/**
 *  This Test Case is for Mention
 
 Inputted:@bob @john (success) such a cool feature
 Expected Result:
                {
                    "emoticons" : [
                        "success"
                    ],
                    "mentions" : [
                        "bob",
                        "john"
                    ]
                }
 */
- (void)testMentionChatMessageCombineOthers
{
    self.homeVCTestCase.messageTextView.text = @"@bob @john (success) such a cool feature";
    CommentObject* curCommentObject = [self.homeVCTestCase createAMessageObjectIsAnswer:YES];
    
    NSDictionary* resultDict = [NSDictionary dictionaryWithObjectsAndKeys:@[@"bob", @"john"], @"mentions", @[@"success"], @"emoticons", nil];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:resultDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString * expectedString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding
                                 ];
    expectedString = [expectedString stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
    XCTAssertEqualObjects(expectedString, curCommentObject.message, @"Data Match!!!");
}

/**
 *  This Test Case is for Mention
 
 Inputted:@chris you around @Ben. @Hung really miss you?
 Expected Result:
                {
                    "mentions" : [
                        "chris",
                        "Ben",
                        "Hung"
                    ]
                }
 */
- (void)testMentionChatMessageMultipleMention
{
    self.homeVCTestCase.messageTextView.text = @"@chris you around @Ben. @Hung really miss you?";
    CommentObject* curCommentObject = [self.homeVCTestCase createAMessageObjectIsAnswer:YES];
    
    NSDictionary* resultDict = [NSDictionary dictionaryWithObjectsAndKeys:@[@"chris", @"Ben", @"Hung"], @"mentions", nil];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:resultDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString * expectedString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding
                                 ];
    expectedString = [expectedString stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
    XCTAssertEqualObjects(expectedString, curCommentObject.message, @"Data Match!!!");
}

/**
 *  This Test Case is for Mention
 
 Inputted:Hello guys @@@@@Ben @@@@Chris?
 Expected Result:
                {
 
                }
 */
- (void)testMentionChatMessageAbNormalCase
{
    self.homeVCTestCase.messageTextView.text = @"Hello guys @@@@@Ben @@@@Chris?";
    CommentObject* curCommentObject = [self.homeVCTestCase createAMessageObjectIsAnswer:YES];
    
    NSDictionary* resultDict = [NSDictionary dictionary];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:resultDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString * expectedString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding
                                 ];
    expectedString = [expectedString stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
    XCTAssertEqualObjects(expectedString, curCommentObject.message, @"Data Match!!!");
}

/**
 *  This Test Case is for Mention
 
 Inputted:Hello remember this email @triDH@Gmail.com?
 Expected Result:
 {
 
 }
 */
- (void)testMentionChatMessageEmailAbnormalCase
{
    self.homeVCTestCase.messageTextView.text = @"Hello remember this email @triDH@Gmail.com?";
    CommentObject* curCommentObject = [self.homeVCTestCase createAMessageObjectIsAnswer:YES];
    
    NSDictionary* resultDict = [NSDictionary dictionary];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:resultDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString * expectedString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding
                                 ];
    expectedString = [expectedString stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
    XCTAssertEqualObjects(expectedString, curCommentObject.message, @"Data Match!!!");
}

/**
 *  This Test Case is for Mention
 
 Inputted:Hello remember this email @triDH@Gmail.com?
 Expected Result:
 {
 
 }
 */
- (void)testMentionChatMessageEmail
{
    self.homeVCTestCase.messageTextView.text = @"Hello remember this email triDH@Gmail.com?";
    CommentObject* curCommentObject = [self.homeVCTestCase createAMessageObjectIsAnswer:YES];
    
    NSDictionary* resultDict = [NSDictionary dictionary];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:resultDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString * expectedString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding
                                 ];
    expectedString = [expectedString stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
    XCTAssertEqualObjects(expectedString, curCommentObject.message, @"Data Match!!!");
}

/**
 *  This Test Case is for Mention
 
 Inputted:Hello remember this email @triDH @Gmail.com?
 Expected Result:
                {
                    "mentions" : [
                        "triDH",
                        "Gmail"
                    ]
                }
 */
- (void)testMentionChatMessageAnotherCase
{
    self.homeVCTestCase.messageTextView.text = @"Hello remember this email @triDH @Gmail.com?";
    CommentObject* curCommentObject = [self.homeVCTestCase createAMessageObjectIsAnswer:YES];
    
    NSDictionary* resultDict = [NSDictionary dictionaryWithObjectsAndKeys:@[@"triDH", @"Gmail"], @"mentions", nil];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:resultDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString * expectedString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding
                                 ];
    expectedString = [expectedString stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
    XCTAssertEqualObjects(expectedString, curCommentObject.message, @"Data Match!!!");
}

#pragma mark - Emotionicon TestCase

/**
 *  This Test Case is for Emoticons
 
 Inputted:Good morning! (megusta) (coffee)?
 Expected Result:
                    {
                        "emoticons" : [
                            "megusta",
                            "coffee"
                        ]
                    }
 */
- (void)testEmotionIconNormalCase
{
    self.homeVCTestCase.messageTextView.text = @"Good morning! (megusta) (coffee)?";
    CommentObject* curCommentObject = [self.homeVCTestCase createAMessageObjectIsAnswer:YES];
    
    NSDictionary* resultDict = [NSDictionary dictionaryWithObjectsAndKeys:@[@"megusta", @"coffee"], @"emoticons", nil];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:resultDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString * expectedString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding
                                 ];
    expectedString = [expectedString stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
    XCTAssertEqualObjects(expectedString, curCommentObject.message, @"Data Match!!!");
}

/**
 *  This Test Case is for Emoticons
 
 Inputted:Good morning! (asdfghjklzxcvbnm) (coffee)?
 Expected Result:
 {
 "emoticons" : [
 "coffee"
 ]
 }
 */
- (void)testEmotionIconCaseOverCharacters
{
    self.homeVCTestCase.messageTextView.text = @"Good morning! (asdfghjklzxcvbnm) (coffee)?";
    CommentObject* curCommentObject = [self.homeVCTestCase createAMessageObjectIsAnswer:YES];
    
    NSDictionary* resultDict = [NSDictionary dictionaryWithObjectsAndKeys:@[@"coffee"], @"emoticons", nil];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:resultDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString * expectedString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding
                                 ];
    expectedString = [expectedString stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
    XCTAssertEqualObjects(expectedString, curCommentObject.message, @"Data Match!!!");
}

#pragma mark - Links TestCase

/**
 *  This Test Case is for Links Extraction
 
 Inputted:@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016
 Expected Result:
 {
 "emoticons" : [
 "success"
 ],
 "links" : [
 {
 "url" : "https://twitter.com/jdorfman/status/430511497475670016",
 "title" : "Justin Dorfman on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;"
 }
 ],
 "mentions" : [
 "bob",
 "john"
 ]
 }

 */
- (void)testLinksCaseNormalCase
{
    self.homeVCTestCase.messageTextView.text = @"@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016";
    CommentObject* curCommentObject = [self.homeVCTestCase createAMessageObjectIsAnswer:YES];
    
    NSArray* linkArray = @[[NSDictionary dictionaryWithObjectsAndKeys:@"https://twitter.com/jdorfman/status/430511497475670016", @"url", @"Justin Dorfman on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;", @"title", nil]];
    
    NSDictionary* resultDict = [NSDictionary dictionaryWithObjectsAndKeys:@[@"bob", @"john"], @"mentions", @[@"success"], @"emoticons", linkArray, @"links", nil];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:resultDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString * expectedString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding
                                 ];
    expectedString = [expectedString stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
    XCTAssertEqualObjects(expectedString, curCommentObject.message, @"Data Match!!!");
}

- (void)testLinksCaseMultipleLinks
{
    self.homeVCTestCase.messageTextView.text = @"@bob @john (success) such a cool feature; https://www.hipchat.com/emoticons https://twitter.com/jdorfman/status/430511497475670016";
    CommentObject* curCommentObject = [self.homeVCTestCase createAMessageObjectIsAnswer:YES];
    
    NSArray* linkArray = @[[NSDictionary dictionaryWithObjectsAndKeys:@"https://www.hipchat.com/emoticons", @"url", @"HipChat - Emoticons", @"title", nil] , [NSDictionary dictionaryWithObjectsAndKeys:@"https://twitter.com/jdorfman/status/430511497475670016", @"url", @"Justin Dorfman on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;", @"title", nil]];
    
    NSDictionary* resultDict = [NSDictionary dictionaryWithObjectsAndKeys:@[@"bob", @"john"], @"mentions", @[@"success"], @"emoticons", linkArray, @"links", nil];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:resultDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString * expectedString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding
                                 ];
    expectedString = [expectedString stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
    XCTAssertEqualObjects(expectedString, curCommentObject.message, @"Data Match!!!");
}

@end
